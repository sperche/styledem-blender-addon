import os

import base64
import logging
from argparse import Namespace

import torch
import cv2
import numpy as np
from skimage.exposure import match_histograms
import zlib

from flask import Flask, request
# import redis

from ...styledem.options.test_options import TestOptions
from ...styledem.models.psp import pSp
from ...utils import dotdict, normalize, Timer


# TODO: remove global variable in the entire file
app = Flask(__name__)
app.logger.setLevel(logging.DEBUG)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# TODO
# r = redis.Redis(host='localhost', port=6379, db=0, decode_responses=True)

def load_weight(filename):
    test_opts = TestOptions().get_opts()
    test_opts.checkpoint_path = filename
    ckpt = torch.load(test_opts.checkpoint_path, map_location=device)
    opts = ckpt['opts']
    opts.update(test_opts)

    if 'learn_in_w' not in opts:
        opts['learn_in_w'] = False
    if 'output_size' not in opts:
        opts['output_size'] = 1024
    opts = Namespace(**opts)

    net = pSp(opts).to(device)
    net.eval()

    return net

# Load the model
path_weights = os.path.join(os.path.dirname(__file__), '..', '..', 'styledem', 'checkpoints', 'best_model_30m_old_data.pt')
net = load_weight(path_weights)

def generate_terrain(use_fp16=True, **kwargs):
    with torch.autocast(device_type=device.type, enabled=use_fp16):
        return net(**kwargs)


def postprocess(output):
    output = normalize(output[0, 0, :, :].cpu().detach().numpy(), high_range=65535)
    img = np.ascontiguousarray(output).astype(np.uint16)
    b64 = base64.b64encode(zlib.compress(img)).decode('utf-8')
    return b64, img.shape


# TODO: this function is copy pasted from the local class
def prepare_input(img: np.ndarray):
        img = normalize(img.astype(np.float64))
        if img.shape[0] != 256:
            img = cv2.resize(img, (256, 256))
        img = torch.from_numpy(img)

        if len(img.shape) == 2:
            img = torch.unsqueeze(img, 0)

        return torch.unsqueeze(img, 0).float().to(device)


def parse_request(request):
    data = request.json

    input_zlib = base64.b64decode(data['input'])
    input_data = np.frombuffer(zlib.decompress(input_zlib),
                                dtype=np.float32).reshape(data['input_shape'])
    
    mask_data = None
    if 'mask' in data:
        mask_zlib = base64.b64decode(data['mask'])
        mask_data = np.frombuffer(zlib.decompress(mask_zlib),
                                    dtype=np.float32).reshape(data['mask_shape'])

    # Set sampling options
    f_res = request.args.get('f_res', default=None, type=int)
    histogram_matching = request.args.get('histogram_matching', default=False, type=lambda v: v.lower() == 'true')
    
    x = prepare_input(input_data)

    return dotdict(dict(x=x, f_res=f_res, mask=mask_data, histogram_matching=histogram_matching))


@app.route("/inversion", methods=['POST'])
def inversion():
    args = parse_request(request)

    if args.mask is not None:
        args.mask = torch.tensor(args.mask).unsqueeze(0).unsqueeze(0).to(device)
        args.mask = torch.nn.functional.interpolate(args.mask, scale_factor=1 / (
                    2 ** (np.log2(args.mask.shape[-1]) - np.log2(args.f_res))), mode='area')
        args.mask = 1 - args.mask

    # TODO: make f_res as a parameter of net.forward
    if args.f_res not in [8, 16, 32, 64, 128]:
        args.f_res = None
    net.f_res = args.f_res
    with torch.no_grad():
        #TODO: make randomize_noise as parameter
        with Timer('inversion'):
            result = net(args.x, resize=False, mask=args.mask, randomize_noise=False)

    if args.histogram_matching:
        result = torch.tensor(match_histograms(result[0, 0, :, :].cpu().detach().numpy(), args.x[0, 0, :, :].cpu().detach().numpy())).unsqueeze(0).unsqueeze(0)
    result_b64, shape = postprocess(result)

    to_return = {'image': result_b64, 'shape': shape}
    return to_return


@app.route("/stylemixing", methods=['POST'])
def stylemixing():
    args = parse_request(request)

    data = request.json

    f_res_style = request.args.get('f_res_style', default=None, type=int)

    style_zlib = base64.b64decode(data['style'])
    style_data = np.frombuffer(zlib.decompress(style_zlib),
                                dtype=np.float32).reshape(data['style_shape'])
    style = prepare_input(style_data)

    if args.mask is not None:
        args.mask = torch.tensor(args.mask).unsqueeze(0).unsqueeze(0).to(device)
        # args.mask = torch.nn.functional.interpolate(args.mask, scale_factor=1 / (
        #             2 ** (np.log2(args.mask.shape[-1]) - np.log2(args.f_res))), mode='area')

    # TODO: make f_res as a parameter of net.forward
    if args.f_res not in [8, 16, 32, 64, 128]:
        args.f_res = None
    if f_res_style not in [8, 16, 32, 64, 128]:
        f_res_style = None
    
    with Timer('style mixing'):
        with torch.no_grad():
        #TODO: make randomize_noise as parameter
            net.f_res = args.f_res
            w1, f_maps_and_img = net(args.x, resize=False, only_get_f_img_and_w=True)
            w2, _ = net(style, resize=False, only_get_f_img_and_w=True)
            net.f_res = f_res_style
            _, f_maps_and_img_style = net(args.x, resize=False, only_get_f_img_and_w=True)

            result = net.decoder.synthesis.style_mixing(w1, w2, args.mask, f_maps_and_img=f_maps_and_img, 
                                                    f_maps_and_img_style=f_maps_and_img_style, 
                                                    start_at=args.f_res, start_at_style=f_res_style, noise_mode='const')

    if args.histogram_matching:
        result = torch.tensor(match_histograms(result[0, 0, :, :].cpu().detach().numpy(), args.x[0, 0, :, :].cpu().detach().numpy())).unsqueeze(0).unsqueeze(0)
    result_b64, shape = postprocess(result)

    to_return = {'image': result_b64, 'shape': shape}
    return to_return


# @app.route("/sketches-step-by-step", methods=['POST'])
# def sketches_step_by_step():
#     print('sketches_step_by_step')
#     running_name = f'{request.remote_addr}.running'
#     stop_name = f'{request.remote_addr}.stop'

#     model_kwargs = parse_request(request)

#     def generate():
#         output_generator = generate_terrain(
#             return_feedback=True,
#             **model_kwargs
#         )

#         # TODO: need testing
#         # r.set(running_name, true)
#         for output in output_generator:
#             b64, shape = postprocess(output.images[0])
#             yield b64 + '\n'

#         # TODO: need testing
#         # r.set(running_name, false)

#     return generate()


# @app.route("/extract-sketch", methods=['POST'])
# def extract_sketch():
#     data = request.json
#     img_zlib = base64.b64decode(data['dem'])
#     terrain_range = request.args.get('range', default=500, type=float)
#     detailed_percentage = 1 - \
#         request.args.get('detailed', default=0.5, type=float)

#     dem = np.frombuffer(zlib.decompress(img_zlib), dtype=np.float32).reshape(
#         data['shape']) * terrain_range
#     sketch = generate_conditioning(dem, 20, base_threshold_range=(
#         detailed_percentage, detailed_percentage))
#     sketch = cv2.resize(sketch, dem.shape)

#     return {'sketch': base64.b64encode(zlib.compress(np.ascontiguousarray(np.asarray(sketch)))).decode('utf-8'),
#             'shape': sketch.shape}


def run_server_api():
    print('Starting...')
    app.run(host='0.0.0.0', port=8030, debug=False)


if __name__ == "__main__":
    run_server_api()
