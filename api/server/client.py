import requests

import numpy as np

from ... import utils
from ..wrapper import StyleDEMAPI

PORT = 8030

class Client(StyleDEMAPI):
    def __init__(self):
        #TODO: make url a parameter
        self.url = f'http://134.214.142.219:{PORT}/'
    
    def invert(self, x: np.ndarray, f_res: int = 16, mask: np.ndarray = None, hist_matching=False):
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        params = {'f_res': f_res, 'histogram_matching': hist_matching}
        x_b64 = utils.nparray_to_base64zlib(x.astype(np.float32))

        url = self.__get_url('inversion', params)

        json={'input': x_b64, 'input_shape': x.shape}

        if mask is not None:
            mask_b64 = utils.nparray_to_base64zlib(mask.astype(np.float32))
            json['mask'] = mask_b64
            json['mask_shape'] = mask.shape

        response = requests.post(url, headers=headers, json=json)

        try:
            data = response.json()
        except requests.exceptions.RequestException:
            print(response.text)
            return None
        else:
            shape = data['shape']
            img_b64 = data['image']
            img_output = utils.base64zlib_to_nparray(img_b64, shape, dtype=np.uint16).astype(np.float32) / 65535.

        return img_output

    def interpolate(self, x: np.ndarray, y: np.ndarray, alpha: float):
        pass

    def style_mixing(self, x: np.ndarray, style: np.ndarray, mask: int, f_res: int = 16, f_res_style: int = 16, hist_matching=False):
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        params = {'f_res': f_res, 'f_res_style': f_res_style, 'histogram_matching': hist_matching}
        x_b64 = utils.nparray_to_base64zlib(x.astype(np.float32))
        style_b64 = utils.nparray_to_base64zlib(style.astype(np.float32))
        mask_b64 = utils.nparray_to_base64zlib(mask.astype(np.float32))

        url = self.__get_url('stylemixing', params)

        json={'input': x_b64, 'input_shape': x.shape, 'style': style_b64, 'style_shape': style.shape,
              'mask': mask_b64, 'mask_shape': mask.shape}

        response = requests.post(url, headers=headers, json=json)

        try:
            data = response.json()
        except requests.exceptions.RequestException:
            print(response.text)
            return None
        else:
            shape = data['shape']
            img_b64 = data['image']
            img_output = utils.base64zlib_to_nparray(img_b64, shape, dtype=np.uint16).astype(np.float32) / 65535.

        return img_output

    def __get_url(self, path, params):
        #TODO: quick hack to change the model
        self.url = f'http://134.214.142.219:{PORT}/'
        return f'{self.url}{path}?{"&".join([f"{k}={v}" for k, v in params.items()])}'
