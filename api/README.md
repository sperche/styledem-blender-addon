# API Folder

This folder contains multiple files that are not used anymore. These come from an old version of the addon where the network ran on a server and the addon communicates through a web server. We left the code "as is", in case someone needs this feature, although it does not appear in the addon settings and has not been tested with the new version.