from abc import ABCMeta, abstractmethod

import numpy as np


class StyleDEMAPI(metaclass=ABCMeta):
    @abstractmethod
    def invert(self, x: np.ndarray, f_res: int = 16, mask: np.ndarray = None, hist_matching=False):
        pass

    @abstractmethod
    def interpolate(self, x: np.ndarray, y: np.ndarray, alpha: float):
        pass

    @abstractmethod
    def style_mixing(self, x: np.ndarray, style: np.ndarray, mask: int, f_res: int = 16, hist_matching=False):
        pass

    # @abstractmethod
    # def load_weight(self, filename: str):
    #     pass
