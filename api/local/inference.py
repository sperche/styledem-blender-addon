import os.path
import tempfile
from copy import deepcopy

# from natsort import natsorted
# from sklearn.linear_model import LinearRegression
import torch
import cv2
import numpy as np
from argparse import Namespace
from skimage.exposure import match_histograms

# from styledem_blender_addon.utils import InferenceType, num_range, Singleton
from ..wrapper import StyleDEMAPI
from ...utils import normalize
from ...styledem.options.test_options import TestOptions
from ...styledem.models.psp import pSp


class Inference(StyleDEMAPI):
    def __init__(self):
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.net = None
        self.opts = None
        self.net = None
        self.weights_loaded = False
    
    def invert(self, x: np.ndarray, f_res: int = 16, mask: np.ndarray = None, hist_matching=False):
        input_img = self.__prepare_input(x)

        if mask is not None:
            mask = torch.tensor(mask.astype(np.float32)).unsqueeze(0).unsqueeze(0).to('cuda')
            mask = torch.nn.functional.interpolate(mask, scale_factor=1 / (
                        2 ** (np.log2(mask.shape[-1]) - np.log2(f_res))), mode='area')
            mask = 1 - mask
 
        # TODO: make f_res as a parameter of net.forward
        if f_res not in [8, 16, 32, 64, 128]:
            f_res = None
        self.net.f_res = f_res
        with torch.no_grad():
            result = self.net(input_img, resize=False, mask=mask)

        if hist_matching:
            result = torch.tensor(match_histograms(result[0, 0, :, :].cpu().detach().numpy(), input_img[0, 0, :, :].cpu().detach().numpy())).unsqueeze(0).unsqueeze(0)

        return self.__prepare_result(result)


    def interpolate(self, x: np.ndarray, y: np.ndarray, alpha: float):
        pass

    def style_mixing(self, x: np.ndarray, style: np.ndarray, mask: int, f_res: int = 16, f_res_style: int = 16, hist_matching=False):
        x = self.__prepare_input(x)
        style = self.__prepare_input(style)

        if mask is not None:
            mask = torch.tensor(mask.astype(np.float32)).unsqueeze(0).unsqueeze(0).to('cuda')
            # args.mask = torch.nn.functional.interpolate(args.mask, scale_factor=1 / (
            #             2 ** (np.log2(args.mask.shape[-1]) - np.log2(args.f_res))), mode='area')

        # TODO: make f_res as a parameter of net.forward
        if f_res not in [8, 16, 32, 64, 128]:
            f_res = None
        if f_res_style not in [8, 16, 32, 64, 128]:
            f_res_style = None
        
        with torch.no_grad():
            #TODO: make randomize_noise as parameter
            self.net.f_res = f_res
            w1, f_maps_and_img = self.net(x, resize=False, only_get_f_img_and_w=True)
            w2, _ = self.net(style, resize=False, only_get_f_img_and_w=True)
            self.net.f_res = f_res_style
            _, f_maps_and_img_style = self.net(x, resize=False, only_get_f_img_and_w=True)

            result = self.net.decoder.synthesis.style_mixing(w1, w2, mask, f_maps_and_img=f_maps_and_img, 
                                                        f_maps_and_img_style=f_maps_and_img_style, 
                                                        start_at=f_res, start_at_style=f_res_style, noise_mode='const')

        if hist_matching:
            result = torch.tensor(match_histograms(result[0, 0, :, :].cpu().detach().numpy(), input_img[0, 0, :, :].cpu().detach().numpy())).unsqueeze(0).unsqueeze(0)

        return self.__prepare_result(result)

    def load_weight(self, filename):
        if self.net is not None:
            del self.net
        test_opts = TestOptions().get_opts()
        test_opts.checkpoint_path = filename
        ckpt = torch.load(test_opts.checkpoint_path, map_location=self.device)
        opts = ckpt['opts']
        opts.update(test_opts)

        if 'learn_in_w' not in opts:
            opts['learn_in_w'] = False
        if 'output_size' not in opts:
            opts['output_size'] = 1024
        self.opts = Namespace(**opts)

        self.net = pSp(self.opts).to(self.device)
        self.net.eval()

        self.weights_loaded = True

    def __prepare_input(self, img: np.ndarray):
        img = normalize(img.astype(np.float64))
        if img.shape[0] != 256:
            img = cv2.resize(img, (256, 256))
        img = torch.from_numpy(img)

        if len(img.shape) == 2:
            img = torch.unsqueeze(img, 0)

        return torch.unsqueeze(img, 0).float().to(self.device)
    
    def __prepare_result(self, img: torch.tensor) -> np.ndarray:
        return normalize(img[0, 0, :, :].cpu().detach().numpy())


# TODO: delete after completing Inference
# class InferenceToDelete:
#     def __init__(self):
#         self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
#         self.opts = None
#         self.net = None
#         self.weights_loaded = False

#     def run(self, img: np.array, inference_type: InferenceType,
#             style_img: np.array = None, inject_after: int = 9, mask: np.ndarray = None, sr_grid: int = 2,
#             interpolation_alpha = 0.5) -> np.ndarray:

#         # cv2.imwrite(fr'C:\Users\Simon\Desktop\test_input.png', self.__normalize(img, 65535).astype(np.uint16))

#         img_torch = self.__prepare_img(img)

#         with torch.no_grad():
#             if inference_type == InferenceType.INVERSION:
#                 result = self.net(img_torch, randomize_noise=self.opts.randomize_noise, resize=self.opts.resize_outputs)
#             elif inference_type == InferenceType.INTERPOLATION:
#                 first_latent = self.get_latent(img)
#                 second_latent = self.get_latent(style_img)
#                 latent = interpolation_alpha * second_latent + (1 - interpolation_alpha) * first_latent
#                 result = self.net(latent, input_code=True, resize=self.opts.resize_outputs, randomize_noise=self.opts.randomize_noise)
#             elif inference_type == InferenceType.STYLE_MIXING or inference_type == InferenceType.STYLE_BRUSH:
#                 assert style_img is not None, 'Style must be set with style mixing inference.'
#                 style_latent = self.get_latent(style_img)

#                 result = self.net(img_torch, latent_mask=num_range(f'{inject_after}-17'), inject_latent=style_latent,
#                                   alpha=self.opts.mix_alpha, resize=self.opts.resize_outputs, randomize_noise=self.opts.randomize_noise)

#                 if inference_type == InferenceType.STYLE_BRUSH:
#                     mask = self.__normalize(mask)
#                     result = self.__normalize(result[0, 0, :, :].cpu().detach().numpy())
#                     img_tmp = self.__normalize(img)
#                     result = (result * mask) + (img_tmp * (1. - mask))
#             elif inference_type == InferenceType.COPY_PASTE:
#                 copy_img = style_img

#                 mask = cv2.resize(mask.astype(np.uint8), img.shape).astype(bool)
#                 img[mask] = copy_img[mask]

#                 img_torch = self.__prepare_img(img)

#                 result = self.net(img_torch, randomize_noise=self.opts.randomize_noise, resize=self.opts.resize_outputs)
#             elif inference_type == InferenceType.SUPER_RESOLUTION:
#                 # TODO: make a function ? 
#                 # TODO: optimize without temporarydirectory

#                 with tempfile.TemporaryDirectory() as tmp_crop_dir, tempfile.TemporaryDirectory() as tmp_inference_dir, tempfile.TemporaryDirectory() as out_path_results:
                    
#                     low_res_image = img
#                     self.__crop(low_res_image, tmp_crop_dir, low_res_image.shape[0]//sr_grid, 1024, True)

#                     files = natsorted([os.path.join(tmp_crop_dir, f) for f in os.listdir(tmp_crop_dir)
#                                     if os.path.isfile(os.path.join(tmp_crop_dir, f)) and f.endswith('.png')])

#                     print('Inferring...')
#                     for i in range(((sr_grid*2)-1)**2):
#                         # To build in-between images, consecutive files are supposed to be on a grid row by row
#                         row, col = np.unravel_index(i, ((sr_grid*2)-1, (sr_grid*2)-1))

#                         # Simple case : all real_im
#                         if row % 2 == 0 and col % 2 == 0:
#                             file = files[(row//2) * sr_grid + (col // 2)]
#                             real_im = cv2.imread(file, cv2.IMREAD_UNCHANGED).astype(np.float64)
#                             from_im = real_im
#                         elif col % 2 != 0 and row % 2 == 0:  # Half of the left image, half of the right
#                             left_img = files[(row // 2) * sr_grid + ((col-1) // 2)]
#                             right_img = files[(row // 2) * sr_grid + ((col+1) // 2)]

#                             left_img = cv2.imread(left_img, cv2.IMREAD_UNCHANGED).astype(np.float64)
#                             right_img = cv2.imread(right_img, cv2.IMREAD_UNCHANGED).astype(np.float64)

#                             from_im = np.empty(left_img.shape)

#                             middle = left_img.shape[1] // 2
#                             from_im[:, :middle] = left_img[:, middle:]
#                             from_im[:, middle:] = right_img[:, :middle]
#                         elif col % 2 == 0 and row % 2 != 0:  # Half of the above image, half of the below image
#                             above_img = files[((row-1) // 2) * sr_grid + (col// 2)]
#                             below_img = files[((row+1) // 2) * sr_grid + (col // 2)]

#                             above_img = cv2.imread(above_img, cv2.IMREAD_UNCHANGED).astype(np.float64)
#                             below_img = cv2.imread(below_img, cv2.IMREAD_UNCHANGED).astype(np.float64)

#                             from_im = np.empty(above_img.shape)

#                             middle = above_img.shape[0] // 2
#                             from_im[:middle, :] = above_img[middle:, :]
#                             from_im[middle:, :] = below_img[:middle, :]
#                         elif row % 2 != 0 and col % 2 != 0:  # A quarter of the up left, up right, down left and down right image
#                             up_left = files[((row - 1) // 2) * sr_grid + ((col - 1) // 2)]
#                             up_right = files[((row - 1) // 2) * sr_grid + ((col + 1) // 2)]
#                             down_left = files[((row + 1) // 2) * sr_grid + ((col - 1) // 2)]
#                             down_right = files[((row + 1) // 2) * sr_grid + ((col + 1) // 2)]

#                             up_left = cv2.imread(up_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
#                             up_right = cv2.imread(up_right, cv2.IMREAD_UNCHANGED).astype(np.float64)
#                             down_left = cv2.imread(down_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
#                             down_right = cv2.imread(down_right, cv2.IMREAD_UNCHANGED).astype(np.float64)

#                             from_im = np.empty(up_left.shape)

#                             middle_y = up_left.shape[0] // 2
#                             middle_x = up_left.shape[1] // 2

#                             from_im[:middle_y, :middle_x] = up_left[middle_y:, middle_x:]
#                             from_im[:middle_y, middle_x:] = up_right[middle_y:, :middle_x]
#                             from_im[middle_y:, :middle_x] = down_left[:middle_y, middle_x:]
#                             from_im[middle_y:, middle_x:] = down_right[:middle_y, :middle_x]

#                         input_img = self.__prepare_img(from_im)
#                         result = self.net(input_img, randomize_noise=self.opts.randomize_noise, resize=self.opts.resize_outputs)
#                         result = result[0].cpu().detach().transpose(0, 2).transpose(0, 1).numpy()
#                         result = self.__normalize(result[:, :, 0], 65535)

#                         result = self.__retarget_dynamic(result, from_im).astype(np.uint16)

#                         im_save_path = os.path.join(tmp_inference_dir, f'{os.path.basename(file)}_{i}.png')
#                         cv2.imwrite(im_save_path, result)

#                     print('Blending...')
#                     self.__blend_intermediate(tmp_inference_dir, out_path_results, sr_grid)

#                     print('Creating the final image...')
#                     result = self.__make_collage(out_path_results, sr_grid)
                        
#                     print('Finished!')
#             else:
#                 raise Exception(f'Inference type {inference_type} is not supported yet.')

#         if inference_type != InferenceType.SUPER_RESOLUTION and inference_type != InferenceType.STYLE_BRUSH:
#             result = result[0, 0, :, :].cpu().detach().numpy()

#         # cv2.imwrite(fr'C:\Users\Simon\Desktop\test_output.png', self.__normalize(result, 65535).astype(np.uint16))

#         return self.__normalize(result)

#     def get_latent(self, img: np.ndarray):
#         img = self.__prepare_img(img)
#         _, latent = self.net(img, return_latents=True)
#         return latent

#     def load_weight(self, filename):
#         if self.net is not None:
#             del self.net
#         test_opts = TestOptions().get_opts()
#         test_opts.checkpoint_path = filename
#         ckpt = torch.load(test_opts.checkpoint_path, map_location=self.device)
#         opts = ckpt['opts']
#         opts.update(test_opts)

#         if 'learn_in_w' not in opts:
#             opts['learn_in_w'] = False
#         if 'output_size' not in opts:
#             opts['output_size'] = 1024
#         self.opts = Namespace(**opts)

#         self.net = pSp(self.opts).to(self.device)
#         self.net.eval()

#         self.weights_loaded = True

#     @staticmethod
#     def __normalize(array: np.ndarray, high_range: float = 1):
#         return ((array - np.min(array)) / (np.max(array) - np.min(array))) * high_range

#     def __prepare_img(self, img: np.ndarray):
#         img = self.__normalize(img.astype(np.float64))
#         if img.shape[0] != 256:
#             img = cv2.resize(img, (256, 256))
#         img = torch.from_numpy(img)

#         if len(img.shape) == 2:
#             img = torch.unsqueeze(img, 0)

#         return torch.unsqueeze(img, 0).float().to(self.device)

#     def __crop(self, img: np.ndarray, out_folder: str, size: int, resize: int, normalize_output: bool):
#         n_x = img.shape[0] // size
#         n_y = img.shape[1] // size
#         for i in range(n_x):
#             for j in range(n_y):
#                 crop_img = img[i * size:i * size + size, j * size:j * size + size]
#                 if resize is not None:
#                     crop_img = cv2.resize(crop_img, (resize, resize))
#                 if normalize_output:
#                     crop_img = self.__normalize(crop_img, 65535)
#                 out = os.path.join(out_folder, f'{i:04d}_{j:04d}.png')
#                 cv2.imwrite(out, crop_img.astype(np.uint16))

#     def __retarget_dynamic(self, x: np.ndarray, y: np.ndarray):
#         x_flat = x.reshape((-1, 1))
#         y_flat = y.flatten()
#         model = LinearRegression().fit(x_flat, y_flat)
#         res = x * model.coef_ + model.intercept_

#         # Overflow might happen if min or max of y are around the range limits
#         if np.min(res) < 0 and np.max(res) > 65535:
#             res = self.__normalize(res, 65535)
#         elif np.min(res) < 0:
#             res = ((res - np.min(res)) / (65535 - np.min(res))) * 65535
#         elif np.max(res) > 65535:
#             res = ((res - 0) / (np.max(res) - 0)) * 65535

#         return res.reshape(x.shape)

#     def __blend_intermediate(self, in_folder: str, out_folder: str, sr_grid_size: int):
#         if not os.path.isdir(out_folder):
#             os.mkdir(out_folder)

#         files = natsorted([os.path.join(in_folder, f) for f in os.listdir(in_folder)
#                         if os.path.isfile(os.path.join(in_folder, f)) and f.endswith('.png')])

#         sr_img = cv2.imread(files[0], cv2.IMREAD_UNCHANGED)
#         mask = np.empty(sr_img.shape)

#         d_max = np.sqrt((sr_img.shape[0] // 2 - 0) ** 2)

#         def wyvill(x):
#             if x < 0:
#                 return 0
#             elif x > 1:
#                 return 1

#             return -(4 / 9) * x * x * x * x * x * x + (17 / 9) * x * x * x * x - (22 / 9) * x * x + 1

#         inner_radius = 0.2
#         outer_radius = 0.8
#         for i in range(sr_img.shape[0]):
#             for j in range(sr_img.shape[1]):
#                 # Circle
#                 d = np.sqrt((i - sr_img.shape[0] // 2) ** 2 + (j - sr_img.shape[1] // 2) ** 2)
#                 d /= d_max

#                 d = (d - inner_radius) / (outer_radius - inner_radius) if inner_radius < d < outer_radius else 0 if d < inner_radius else 1
#                 d = wyvill(d)

#                 mask[i, j] = d

#         m_y = sr_img.shape[0] // 2
#         m_x = sr_img.shape[1] // 2

#         final_mask = deepcopy(mask)
#         final_mask[:m_y, :m_x] += mask[m_y:, m_x:] + mask[m_y:, :m_x] + mask[:m_y, m_x:]
#         final_mask[:m_y, m_x:] += mask[m_y:, m_x:] + mask[m_y:, :m_x] + mask[:m_y, :m_x]
#         final_mask[m_y:, :m_x] += mask[m_y:, m_x:] + mask[:m_y, m_x:] + mask[:m_y, :m_x]
#         final_mask[m_y:, m_x:] += mask[m_y:, :m_x] + mask[:m_y, m_x:] + mask[:m_y, :m_x]

#         cv2.imwrite(os.path.join(out_folder, f'mask.png'), (mask*255).astype(np.uint8))

#         inference_grid_size = (sr_grid_size * 2) - 1
#         for i in range(((sr_grid_size * 2) - 1) ** 2):
#             # To build in-between images, consecutive files are supposed to be on a grid row by row
#             row, col = np.unravel_index(i, ((sr_grid_size * 2) - 1, (sr_grid_size * 2) - 1))

#             # Only blend real images (not intermediate)
#             if row % 2 != 0 or col % 2 != 0:
#                 continue

#             middle = files[row * inference_grid_size + col]
#             imgs = np.zeros((sr_img.shape[0], sr_img.shape[1], 9))
#             imgs[:, :, 4] = cv2.imread(middle, cv2.IMREAD_UNCHANGED)

#             # Up, down, right and left cases
#             if row != 0:
#                 up = files[(row - 1) * inference_grid_size + col]
#                 imgs[:, :, 1] = cv2.imread(up, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             else:
#                 imgs[m_y:, :, 1] += imgs[:m_y, :, 4]

#             if col != 0:
#                 left = files[row * inference_grid_size + (col - 1)]
#                 imgs[:, :, 3] = cv2.imread(left, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             else:
#                 imgs[:, m_x:, 3] += imgs[:, :m_x, 4]

#             if col != inference_grid_size - 1:
#                 right = files[row * inference_grid_size + (col + 1)]
#                 imgs[:, :, 5] = cv2.imread(right, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             else:
#                 imgs[:, :m_x, 5] += imgs[:, m_x:, 4]

#             if row != inference_grid_size - 1:
#                 down = files[(row + 1) * inference_grid_size + col]
#                 imgs[:, :, 7] = cv2.imread(down, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             else:
#                 imgs[:m_y, :, 7] += imgs[m_y:, :, 4]

#             # Up-left, down-left, up-right and down-right cases
#             # It might probably be rewritten more concisely but I find it clearer (sort of) to detail each case
#             if col != 0 and row != 0:
#                 up_left = files[(row - 1) * inference_grid_size + (col - 1)]
#                 imgs[:, :, 0] = cv2.imread(up_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             elif col == 0 and row == 0:
#                 imgs[m_y:, m_x:, 0] += imgs[:m_y, :m_x, 4]
#             elif col != 0 and row == 0:
#                 imgs[m_y:, m_x:, 0] += imgs[:m_y, m_x:, 3]
#             elif col == 0 and row != 0:
#                 imgs[m_y:, m_x:, 0] += imgs[m_y:, :m_x, 1]

#             if col != inference_grid_size - 1 and row != 0:
#                 up_right = files[(row - 1) * inference_grid_size + (col + 1)]
#                 imgs[:, :, 2] = cv2.imread(up_right, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             elif col == inference_grid_size - 1 and row == 0:
#                 imgs[m_y:, :m_x, 2] += imgs[:m_y, m_x:, 4]
#             elif col != inference_grid_size - 1 and row == 0:
#                 imgs[m_y:, :m_x, 2] += imgs[:m_y, :m_x, 5]
#             elif col == inference_grid_size - 1 and row != 0:
#                 imgs[m_y:, :m_x, 2] += imgs[m_y:, m_x:, 1]

#             if col != 0 and row != inference_grid_size - 1:
#                 down_left = files[(row + 1) * inference_grid_size + (col - 1)]
#                 imgs[:, :, 6] = cv2.imread(down_left, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             elif col == 0 and row == inference_grid_size - 1:
#                 imgs[:m_y, m_x:, 6] += imgs[m_y:, :m_x, 4]
#             elif col == 0 and row != inference_grid_size - 1:
#                 imgs[:m_y, m_x:, 6] += imgs[:m_y, :m_x, 7]
#             elif col != 0 and row == inference_grid_size - 1:
#                 imgs[:m_y, m_x:, 6] += imgs[m_y:, m_x:, 3]

#             if col != inference_grid_size - 1 and row != inference_grid_size - 1:
#                 down_right = files[(row + 1) * inference_grid_size + (col + 1)]
#                 imgs[:, :, 8] = cv2.imread(down_right, cv2.IMREAD_UNCHANGED).astype(np.float64)
#             elif col == inference_grid_size - 1 and row == inference_grid_size - 1:
#                 imgs[:m_y, :m_x, 8] += imgs[m_y:, m_x:, 4]
#             elif col == inference_grid_size - 1 and row != inference_grid_size - 1:
#                 imgs[:m_y, :m_x, 8] += imgs[:m_y, m_x:, 7]
#             elif col != inference_grid_size - 1 and row == inference_grid_size - 1:
#                 imgs[:m_y, :m_x, 8] += imgs[m_y:, :m_x, 5]

#             imgs *= mask[:, :, np.newaxis]

#             final_img = imgs[:, :, 4]

#             final_img[:m_y, :m_x] += imgs[m_y:, m_x:, 0] + imgs[m_y:, :m_x, 1] + imgs[:m_y, m_x:, 3]
#             final_img[:m_y, m_x:] += imgs[m_y:, m_x:, 1] + imgs[m_y:, :m_x, 2] + imgs[:m_y, :m_x, 5]
#             final_img[m_y:, :m_x] += imgs[m_y:, m_x:, 3] + imgs[:m_y, m_x:, 6] + imgs[:m_y, :m_x, 7]
#             final_img[m_y:, m_x:] += imgs[m_y:, :m_x, 5] + imgs[:m_y, m_x:, 7] + imgs[:m_y, :m_x, 8]

#             final_img /= final_mask

#             cv2.imwrite(os.path.join(out_folder, f'{(row * sr_grid_size + col)}.png'), final_img.astype(np.uint16))

#     def __make_collage(self, in_dir: str, size: int):
#         files = natsorted([os.path.join(in_dir, f) for f in os.listdir(in_dir)
#                         if os.path.isfile(os.path.join(in_dir, f)) and f.endswith('.png')])

#         final_img = None
#         for i in range(size):
#             line_img = None
#             for j in range(size):
#                 img = cv2.imread(files[i * size + j], cv2.IMREAD_UNCHANGED)
#                 if line_img is None:
#                     line_img = np.asarray(img, dtype=np.float64)
#                 else:
#                     line_img = np.concatenate((line_img, img), axis=1)
#             if final_img is None:
#                 final_img = line_img
#             else:
#                 final_img = np.concatenate((final_img, line_img), axis=0)

#         return final_img.astype(np.float64)
