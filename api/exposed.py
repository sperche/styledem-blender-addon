from ..utils import Singleton
# from . import local, server
from .wrapper import StyleDEMAPI

LOCAL = True

if LOCAL:
    from . import local
else:
    from . import server

class API(Singleton):
    styledem_api: StyleDEMAPI
    init: bool = False

    def get_api(self) -> StyleDEMAPI:
        if not self.init:
            if LOCAL:
                self.init_local()
            else:
                self.init_server()
            self.init = True
        return self.styledem_api

    def init_server(self):
        self.styledem_api = server.client.Client()

    def init_local(self):
        self.styledem_api = local.inference.Inference()