import bpy
from bpy_extras.io_utils import ImportHelper

from ..utils import Logger
from ..api import API


class LogFolder(bpy.types.Operator, ImportHelper):
    bl_idname = "styledem.choose_log_folder"
    bl_label = "Choose the log folder"
    use_filter_folder = True

    def execute(self, context):
        Logger().start(self.filepath)
        if not Logger().is_init():
            self.report({'ERROR'}, 'Please choose folder with write permission (on the desktop for example)')

        return {'FINISHED'}


class OpenFileBrowser(bpy.types.Operator, ImportHelper):
    bl_idname = "styledem.open_filebrowser"
    bl_label = "Open the file browser"

    def execute(self, context):
        """Load the weights of the network."""

        Logger().log('Loading weights...')

        print('Selected file:', self.filepath)

        API().get_api().load_weight(self.filepath)
        Logger().log('Weights loaded')

        print('Loaded!')

        return {'FINISHED'}
