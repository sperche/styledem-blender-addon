import numpy as np
import bpy

from ..utils import Logger, InferenceType
from ..api import API
from ..utils_bpy import to_np_array, to_bpy_img
from .. import utils_bpy


class InversionProperty(bpy.types.PropertyGroup):
    input_image: bpy.props.StringProperty(name='Input')
    output_image: bpy.props.StringProperty(name='Output')
    mask_image: bpy.props.StringProperty(name='Mask')

    f_maps_level: bpy.props.EnumProperty(items=[('4', 'Creative (4)', '', 0), ('8', 'Neutral Creative (8)', '', 5), ('16', 'Less creative (16)', '', 1), ('32', 'Neutral (32)', '', 2),
                                                ('64', 'Less accurate (64)', '', 3), ('128', 'Accurate (128)', '', 4)],
                                              description='Fmaps', default='16')
    
    hist_match: bpy.props.BoolProperty(default=False)


class InvertImage(bpy.types.Operator):
    bl_idname = "styledem.invert"
    bl_label = "Invert the image"

    def execute(self, context):
        Logger().log('INVERSION: started...')
        props = context.scene.inversion_props
        img_name = props.input_image
        output_name = props.output_image
        mask_name = props.mask_image
        if not img_name or not output_name or not mask_name:
            self.report({'ERROR'}, 'Please choose an input AND output image first')
            Logger().log('INVERSION ERROR: input or output not setup')
            return {'FINISHED'}

        bpy_img = bpy.data.images[img_name]

        try:
            img = to_np_array(bpy_img)
        except:
            self.report({'ERROR'}, 'The chosen image is empty')
            Logger().log('INVERSION ERROR: Chosen image empty')
            return {'FINISHED'}

        mask = None
        try:
            #TODO: mask as parameter
            mask = to_np_array(bpy.data.images[mask_name])
        except:
            print('No mask set')
        
        inverted_img = API().get_api().invert(img, f_res=int(props.f_maps_level), mask=mask, hist_matching=props.hist_match)

        to_bpy_img(inverted_img, output_name)
        utils_bpy.update_2d_3d_views_img(output_name)

        Logger().log_image(img, 'inversion_input')
        Logger().log_image(inverted_img, 'inversion_output')
        Logger().log('INVERSION: Done.')

        return {'FINISHED'}
