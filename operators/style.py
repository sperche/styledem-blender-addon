import numpy as np
import bpy

from ..api import API
from ..utils import Logger
from ..utils_bpy import to_np_array, to_bpy_img
from .. import utils_bpy


class StyleMixingProperty(bpy.types.PropertyGroup):
    input_image: bpy.props.StringProperty(name='Input')
    style_image: bpy.props.StringProperty(name='Style')
    mask_image: bpy.props.StringProperty(name='Mask')
    output_image: bpy.props.StringProperty(name='Output')

    f_maps_level: bpy.props.EnumProperty(items=[('4', 'Creative (4)', '', 0), ('8', '(8)', '', 5), ('16', 'Less creative (16)', '', 1), ('32', 'Neutral (32)', '', 2),
                                                ('64', 'Less accurate (64)', '', 3), ('128', 'Accurate (128)', '', 4)],
                                              description='Fmaps', default='16')
    f_maps_level_style: bpy.props.EnumProperty(items=[('4', 'Creative (4)', '', 0), ('8', '(8)', '', 5), ('16', 'Less creative (16)', '', 1), ('32', 'Neutral (32)', '', 2),
                                                ('64', 'Less accurate (64)', '', 3), ('128', 'Accurate (128)', '', 4)],
                                              description='Fmaps style', default='16')

class StyleMixing(bpy.types.Operator):
    bl_idname = "styledem.stylemixing"
    bl_label = "Style mixing"

    def execute(self, context):
        props = context.scene.stylemixing_props

        Logger().log(f'STYLEMIXING: started (f_res = {props.f_maps_level})...')

        img_name = props.input_image
        output_name = props.output_image
        style_name = props.style_image
        mask_name = props.mask_image

        if not img_name or not output_name or not style_name:
            self.report({'ERROR'}, 'Please choose an input AND output AND style first')
            Logger().log('STYLEMIXING ERROR: input or output not setup')
            return {'FINISHED'}

        bpy_img = bpy.data.images[img_name]
        bpy_style = bpy.data.images[style_name]
        mask = to_np_array(bpy.data.images[mask_name])

        # TODO: except proper error
        try:
            img = to_np_array(bpy_img)
            style = to_np_array(bpy_style)
        except:
            self.report({'ERROR'}, 'The chosen image is empty')
            Logger().log('STYLEMIXING ERROR: chosen image empty')
            return {'FINISHED'}

        inverted_img = API().get_api().style_mixing(img, style, mask, 
                                                    f_res=int(props.f_maps_level), f_res_style=int(props.f_maps_level_style), 
                                                    hist_matching=context.scene.inversion_props.hist_match)

        to_bpy_img(inverted_img, output_name)
        utils_bpy.update_2d_3d_views_img(output_name)

        Logger().log_image(img, 'stylemixing_input')
        Logger().log_image(style, 'stylemixing_style')
        Logger().log_image(inverted_img, 'stylemixing_output')
        Logger().log('STYLEMIXING: Done.')

        return {'FINISHED'}
