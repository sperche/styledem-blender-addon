from enum import Enum, auto
from fileinput import filename
import re
from typing import List
import time

import numpy as np
import os
import datetime
import io
import zlib
import base64

# import cv2



class InferenceType(Enum):
    INVERSION = auto()
    STYLE_MIXING = auto()
    STYLE_BRUSH = auto()
    COPY_PASTE = auto()
    SUPER_RESOLUTION = auto()
    INTERPOLATION = auto()


# https://github.com/NVlabs/stylegan2-ada-pytorch/blob/main/generate.py
def num_range(s: str) -> List[int]:
    '''Accept either a comma separated list of numbers 'a,b,c' or a range 'a-c' and return as a list of ints.'''

    range_re = re.compile(r'^(\d+)-(\d+)$')
    m = range_re.match(s)
    if m:
        return list(range(int(m.group(1)), int(m.group(2)) + 1))
    vals = s.split(',')
    return [int(x) for x in vals]


def normalize_vec(a: np.ndarray):
    return a / np.sqrt(np.sum(a ** 2))


def normalize(array: np.ndarray, high_range: float = 1):
    return ((array - np.min(array)) / (np.max(array) - np.min(array))) * high_range


def nparray_to_base64zlib(img: np.ndarray):
    data = zlib.compress(np.ascontiguousarray(img))
    im_b64 = base64.b64encode(data).decode('utf-8')
    return im_b64


def base64zlib_to_nparray(base64_input: str, shape, dtype=np.float32):
    if isinstance(base64_input, str):
        base64_input = base64_input.encode('utf-8')
    else:
        base64_input = base64_input.decode('utf-8')
    img_zlib = base64.b64decode(base64_input)
    img = np.frombuffer(zlib.decompress(img_zlib), dtype=dtype).reshape(shape)
    return img
    

def normalize(array: np.ndarray, high_range: float = 1):
    return ((array - np.min(array)) / (np.max(array) - np.min(array))) * high_range

# https://deepnote.com/@rmi-ppin/Faie-un-singleton-en-python-0d187a73-2f24-4c49-b2aa-bbbf4a45ace6
class Singleton:
    __instance = None

    def __new__(cls,*args, **kwargs):
        if cls.__instance is None :
            cls.__instance = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls.__instance


# https://stackoverflow.com/questions/2352181/how-to-use-a-dot-to-access-members-of-dictionary
class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class Logger(Singleton):
    main_folder: str = './log'
    filename: str = 'log.txt'
    file: io.TextIOWrapper
    counter: int = 0
    init: bool = False

    def start(self, folder):
        self.main_folder = os.path.join(folder, 'logs')
        os.makedirs(self.main_folder, exist_ok=True)
        self.init = True
        Logger().log('Log session started.')

    def is_init(self):
        return self.init

    def log_image(self, img: np.ndarray, prefix: str = ''):
        if not self.init:
            return

        now = datetime.datetime.now()
        np.save(os.path.join(self.main_folder, f'{now.day:02d}_{now.month:02d}_{now.year}_{now.hour:02d}_{now.minute:02d}_{now.second:02d}{f"_{prefix}" if prefix else ""}_log_{self.counter}'), img)
        # TODO: cv2 not available in blender
        # cv2.imwrite(os.path.join(self.main_folder, f'{now.day:02d}_{now.month:02d}_{now.year}_{now.hour:02d}_{now.minute:02d}_{now.second:02d}{f"_{prefix}" if prefix else ""}_log_{self.counter}.png'), normalize(img, 65535).astype(np.uint16))
        self.counter += 1

    def log(self, txt: str):
        if not self.init:
            return

        now = datetime.datetime.now()
        with open(os.path.join(self.main_folder, self.filename), "a") as f:
            f.write(f'{now.day:02d}/{now.month:02d}/{now.year} {now.hour:02d}:{now.minute:02d}:{now.second:02d} - {txt}\n')
        print(txt)


class Timer(object):
    def __init__(self, name=None):
        self.name = name

    def __enter__(self):
        self.tstart = time.time()

    def __exit__(self, type, value, traceback):
        if self.name:
            print('[%s]' % self.name,)
        print('Elapsed: %s' % (time.time() - self.tstart))