import numpy as np
import bpy
from bpy.types import bpy_prop_collection


def to_np_array(bpy_img, grayscale=True):
    change_colorspace = False
    if bpy_img.filepath != '' and bpy_img.colorspace_settings.name == 'sRGB':
        bpy_img.colorspace_settings.name = 'Non-Color'
        bpy_img.reload() 
        change_colorspace = True

    img = np.asarray(bpy_img.pixels)

    if change_colorspace:
        bpy_img.colorspace_settings.name = 'sRGB'
        bpy_img.reload() 

    s = bpy_img.size

    #TODO: raise a proper error
    if s[0] == 0 or s[1] == 0:
        raise Exception
    
    img = np.resize(img, (s[0], s[1], bpy_img.channels))

    if grayscale:
        #TODO : maybe add warning or do the mean of channels
        img = img[:, :, 0]
    else:
        #TODO: keep alpha channel
        img = img[:, :, 0:3]

    return img


def to_bpy_img(img: np.ndarray, name: str):
    img = img.astype(np.float32)
    if len(img.shape) == 2:
        img = img[:, :, np.newaxis]
    if img.shape[2] == 1:
        img = np.repeat(img, 4, axis=2)
        img[:, :, 3] = 1.
    elif img.shape[2] == 3:
        img = np.concatenate((img, np.ones((*img.shape[0:2], 1), dtype=img.dtype)), axis=2)

    height, width = img.shape[1], img.shape[0]
    if name not in bpy.data.images:
        blender_img = bpy.data.images.new(name=name, width=width, height=height, alpha=False, is_data=False)
    else:
        blender_img = bpy.data.images[name]
        blender_img.scale(height, width)

    # Non-color to sRGB
    # img = np.power(img, 2.2)

    blender_img.pixels.foreach_set(img.ravel())

    return blender_img


def search(ID):
    def users(col):
        ret =  tuple(repr(o) for o in col if o.user_of_id(ID))
        return ret if ret else None
    return filter(None, (
        users(getattr(bpy.data, p)) 
        for p in  dir(bpy.data) 
        if isinstance(
                getattr(bpy.data, p, None), 
                bpy_prop_collection
                )                
        )
        )


def update_2d_3d_views_img(name: str):
    if name not in bpy.data.images:
        #TODO: raise or alert user
        return
    
    blender_img = bpy.data.images[name]
    for user_str in search(blender_img):
        user = eval(user_str[0])
        user.update_tag()
    blender_img.update()