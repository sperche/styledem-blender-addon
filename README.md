# Authoring Terrains with Spatialised Style 

__Please note that this repository is not the main source code of the paper but a Blender addon developed for easy use. While having some of the network code, it does not include any training code.__

[Website](https://simonperche.github.io/authoring_terrains_with_spatialised_style/) | [Paper](https://hal.science/hal-04323125v1/document) | [Video](https://www.youtube.com/watch?v=giK-2w_a6DQ) | Code (coming soon)

## Abstract

Various terrain modeling methods have been proposed for the past few decades, providing efficient and often interactive authoring tools. However, they seldom include any notion of style, which is a critical aspect for designers in the entertainment industry. We introduce a new generative network method that bridges the gap between automatic terrain synthesis and authoring, providing a versatile set of authoring tools allowing spatialized style. Given an input sketch or existing elevation map, our method generates a terrain with features that can be authored, enhanced, and augmented using interactive brushes and style manipulation tools. The strength of our approach resides in the versatility and interoperability of the toolbox.

# Version

Please use a version of Blender greater than 2.8.
Tested on Windows 10 and Linux on Blender 3.0.

# Installation 

Our addon needs Blender to directly execute the inference of the network. It requires to install PyTorch directly into the Blender python environment.

## Needed packages
- opencv-python
- matplotlib
- click
- natsort
- sklearn

All packages are listed in `requirements.txt`.
Our versions of each package are given. However, it might works with newer versions.

### Prepare python environment for PyTorch

Go to the installation of Blender into `blender/3.0/python/bin`.
Execute this command:  ```bash ./python3.9 -m pip install -r requirements.txt ```
Troubleshoot Linux
If, while enabling the addon, it throws `module not found` error, execute this command to copy all packages from local folder to blender folder `cp -r $HOME/.local/lib/python3.10/site-packages/* ../lib/python3.10/` (adapt for your version).

## Weights

Please download weights from the [drive folder](https://drive.google.com/drive/folders/1Yxj0SF-6MnmlH5_Ce8j-DXmqyeyG5-Fu?usp=drive_link) or from the release page.

## Blender layout

A layout is provided, alreay set up with all the required tabs. Please find it in the [drive folder](https://drive.google.com/drive/folders/1Yxj0SF-6MnmlH5_Ce8j-DXmqyeyG5-Fu?usp=drive_link) or in the release page.

## Addon

To install the Blender addon, go to Edit->Preferences->Addon. Zip the entire repository (or download the .zip file in the release page) and install the zip.

# Tools

![](docs/screenshot.png)

All image fields in tools must be a 1024x1024 image.

## Inversion

This tool presents the core of the method by recreating the input into the closest output. 
Inputs might be DEM, sketch, edited DEM with sketch. 

Fields : 
- Input : DEM [edited] or sketch
- Mask : white value in mask do not take into account "Features maps levels" parameters and always make a full pass in the StyleGan model. 
- Output : output image. Could be the same as input.
- Features maps level : resolution of the features maps of the input. 4 corresponds to a full pass of the StyleGan model, while 128 produce only small changes.  

Moreover, a network with a different resolution may be loaded to "transform" features from a DEM. For example, a DEM with 30m/pixels of precision could be reinterpreted with the 5m/pixels networks. Location of mountains and valleys will be preserved but features will be changed into a zoom version.

## Style mixing

Apply a style on a terrain or a sketch.

Fields : 

- Input : image where the style will be applied
- Style : image where the style will be extracted
- Features maps level : resolution of the features maps of the input. 4 corresponds to a full pass of the StyleGan model, while 128 produce only small changes.  
- Style F: resolution of the features maps of the style
- Output : output image. Could be the same as input.

# Citation
If you find our work useful, please consider citing:

```
@article{perche2023spatialisedstyle,
  author    = {Perche, Simon and Peytavie, Adrien and Benes, Bedrich and Galin, Eric and Guérin, Eric},
  title     = {Authoring Terrains with Spatialised Style},
  journal   = {Pacific Graphics},
  year      = {2023},
}
