import bpy

from . import settings

# Useful for children of main panel
MAIN_PANEL_NAME = 'STYLEDEM_PT_MainPanel'
INVERSION_PANEL_NAME = 'STYLEDEM_PT_InversionPanel'
STYLEMIXING_PANEL_NAME = 'STYLEDEM_PT_StyleMixingPanel'



class MainPanel(settings.PanelSettings, bpy.types.Panel):
    bl_idname = MAIN_PANEL_NAME
    bl_label = 'StyleDEM'
    bl_options = {"HEADER_LAYOUT_EXPAND"}
    bl_category = 'Terrain'

    def draw(self, context):
        col = self.layout.column()
        inversion_props = context.scene.inversion_props

        col.operator("styledem.choose_log_folder", text='Select a log folder')
        col.operator('styledem.open_filebrowser', text='Open weights')
        col.prop(inversion_props, 'hist_match', text='Histogram matching')
        

class InversionPanel(settings.PanelSettings, bpy.types.Panel):
    bl_parent_id = MAIN_PANEL_NAME
    bl_idname = INVERSION_PANEL_NAME
    bl_label = 'Inversion'
    bl_description = "Invert a DEM or a levelset"
    

    def draw(self, context):
        layout = self.layout
        inversion_props = context.scene.inversion_props

        col = layout.column()

        col.prop_search(inversion_props, 'input_image', bpy.data, 'images')
        col.prop_search(inversion_props, 'mask_image', bpy.data, 'images')
        col.prop_search(inversion_props, 'output_image', bpy.data, 'images')
        col.prop(inversion_props, 'f_maps_level', slider=True, text='Features maps level')
        col.operator('styledem.invert', text='Invert image')
       

class StyleMixingPanel(settings.PanelSettings, bpy.types.Panel):
    bl_parent_id = MAIN_PANEL_NAME
    bl_idname = STYLEMIXING_PANEL_NAME
    bl_label = 'Style mixing'
    bl_description = "Apply style on a DEM"


    def draw(self, context):
        layout = self.layout
        stylemixing_props = context.scene.stylemixing_props

        col = layout.column()

        col.prop_search(stylemixing_props, 'input_image', bpy.data, 'images')
        col.prop_search(stylemixing_props, 'style_image', bpy.data, 'images')
        col.prop_search(stylemixing_props, 'mask_image', bpy.data, 'images')
        col.prop(stylemixing_props, 'f_maps_level', slider=True, text='Features maps level')
        col.prop(stylemixing_props, 'f_maps_level_style', slider=True, text='Style feature maps level')
        col.prop_search(stylemixing_props, 'output_image', bpy.data, 'images')
        col.operator('styledem.stylemixing', text='Apply style')
