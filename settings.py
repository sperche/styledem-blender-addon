try:
    import bpy
except ImportError:
    print('"bpy" module not found. This may not be an issue if you are running the server.')


ADDON_NAME = 'styledem'


class PanelSettings:
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_options = {"DEFAULT_CLOSED"}


class StyleDEMPreferences(bpy.types.AddonPreferences):
    bl_idname = ADDON_NAME

    run_locally: bpy.props.BoolProperty(namedefault=False, description='Run locally or on a server')

    server_url: bpy.props.StringProperty(name='URL', default='http://localhost')
    server_port: bpy.props.StringProperty(name='Port', default='8080')

    def draw(self, context):
        row = self.layout.row()
        row.prop(self, 'run_locally', text='Run locally')
        if not self.run_locally:
            col = self.layout.column()
            col.label(text='Settings of the render server')
            row = col.row()
            row.prop(self, 'server_url')
            row.prop(self, 'server_port')
