dataset_paths = {
    'celeba_train': '',
    'celeba_test': '',
    'celeba_train_sketch': '',
    'celeba_test_sketch': '',
    'celeba_train_segmentation': '',
    'celeba_test_segmentation': '',
    'ffhq': '',
    'dem_5m': 'train_datasets/512/dem_5m',
    'dem_5m_test': 'train_datasets/512/dem_5m_test',
    'dem_5m_1024_train_sketch': '/gpfswork/rech/bpu/uyv77du/datasets/dem_5m_1024/sketch/train',
    'dem_5m_1024_test_sketch': '/gpfswork/rech/bpu/uyv77du/datasets/dem_5m_1024/sketch/test',
    'dem_5m_1024_train': '/mnt/ssd/SVN/StyleGan/datasets/dem_5m_1024/dem/train',
    'dem_5m_1024_test': '/mnt/ssd/SVN/StyleGan/datasets/dem_5m_1024/dem/test',
    'dem_30m_256_train_sketch': '/gpfswork/rech/bpu/uyv77du/datasets/sketch_2017/sketch/train',
    'dem_30m_256_test_sketch': '/gpfswork/rech/bpu/uyv77du/datasets/sketch_2017/sketch/test',
    'dem_30m_256_train': '/mnt/ssd/SVN/StyleGan/datasets/dem_30m_256/dem/train',
    'dem_30m_256_test': '/mnt/ssd/SVN/StyleGan/datasets/dem_30m_256/dem/test',

    'test_overfit_train': '/mnt/ssd/SVN/StyleGan/test/srtm_30m/train',
    'test_overfit_test': '/mnt/ssd/SVN/StyleGan/test/srtm_30m/test',

    'test_w_train': '/media/ssd/SVN/StyleGan/datasets/sg2_srtm_30m_1024_4000kimg_w/dem/train',
    'test_w_test': '/media/ssd/SVN/StyleGan/datasets/sg2_srtm_30m_1024_4000kimg_w/dem/test',
    'test_w_train_w': '/media/ssd/SVN/StyleGan/datasets/sg2_srtm_30m_1024_4000kimg_w/w/train',
    'test_w_test_w': '/media/ssd/SVN/StyleGan/datasets/sg2_srtm_30m_1024_4000kimg_w/w/test',

    'test_train': '/media/hdd/sperche/projects/datasets/sg2_srtm_30m_1024_4000kimg_small',
    'test_test': '/media/hdd/sperche/projects/datasets/sg2_srtm_30m_1024_4000kimg_small',

    'sg2_srtm_30m_1024_4000kimg_train': '/media/hdd/sperche/projects/datasets/sg2_srtm_30m_1024_4000kimg/train',
    'sg2_srtm_30m_1024_4000kimg_test': '/media/hdd/sperche/projects/datasets/sg2_srtm_30m_1024_4000kimg/test',

    'srtm_alps_ethio_train': '/media/hdd/sperche/projects/datasets/alps_ethio_hima_indo_rockies_train_test/train',
    'srtm_alps_ethio_test': '/media/hdd/sperche/projects/datasets/alps_ethio_hima_indo_rockies_train_test/test',
}

model_paths = {
    'stylegan_ffhq': 'pretrained_models/stylegan2-ffhq-config-f.pt',
    'ir_se50': 'pretrained_models/model_ir_se50.pth',
    'circular_face': 'pretrained_models/CurricularFace_Backbone.pth',
    'mtcnn_pnet': 'pretrained_models/mtcnn/pnet.npy',
    'mtcnn_rnet': 'pretrained_models/mtcnn/rnet.npy',
    'mtcnn_onet': 'pretrained_models/mtcnn/onet.npy',
    'shape_predictor': 'shape_predictor_68_face_landmarks.dat',
    'moco': 'pretrained_models/moco_v2_800ep_pretrain.pth.tar',
    'stylegan_dem_5m_128': 'pretrained_models/sg2_dem_128.pkl',
    'stylegan_dem_5m_1024': 'pretrained_models/sg2_dem_1024_batch32.pt',
    'stylegan_dem_30m_256': 'pretrained_models/sg2_dem_30m_246.pt',
}
