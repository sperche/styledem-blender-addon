from . import transforms_config
from .paths_config import dataset_paths

DATASETS = {
    'ffhq_encode': {
        'transforms': transforms_config.EncodeTransforms,
        'train_source_root': dataset_paths['ffhq'],
        'train_target_root': dataset_paths['ffhq'],
        'test_source_root': dataset_paths['celeba_test'],
        'test_target_root': dataset_paths['celeba_test'],
    },
    'ffhq_frontalize': {
        'transforms': transforms_config.FrontalizationTransforms,
        'train_source_root': dataset_paths['ffhq'],
        'train_target_root': dataset_paths['ffhq'],
        'test_source_root': dataset_paths['celeba_test'],
        'test_target_root': dataset_paths['celeba_test'],
    },
    'celebs_sketch_to_face': {
        'transforms': transforms_config.SketchToImageTransforms,
        'train_source_root': dataset_paths['celeba_train_sketch'],
        'train_target_root': dataset_paths['celeba_train'],
        'test_source_root': dataset_paths['celeba_test_sketch'],
        'test_target_root': dataset_paths['celeba_test'],
    },
    'celebs_seg_to_face': {
        'transforms': transforms_config.SegToImageTransforms,
        'train_source_root': dataset_paths['celeba_train_segmentation'],
        'train_target_root': dataset_paths['celeba_train'],
        'test_source_root': dataset_paths['celeba_test_segmentation'],
        'test_target_root': dataset_paths['celeba_test'],
    },
    'celebs_super_resolution': {
        'transforms': transforms_config.SuperResTransforms,
        'train_source_root': dataset_paths['celeba_train'],
        'train_target_root': dataset_paths['celeba_train'],
        'test_source_root': dataset_paths['celeba_test'],
        'test_target_root': dataset_paths['celeba_test'],
    },
    'dem_encode': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['dem_5m_1024_train'],
        'train_target_root': dataset_paths['dem_5m_1024_train'],
        'test_source_root': dataset_paths['dem_5m_1024_test'],
        'test_target_root': dataset_paths['dem_5m_1024_test'],
    },
    'dem_encode_no_resize': {
        'transforms': transforms_config.DEMTransformsNoResize,
        'train_source_root': dataset_paths['dem_5m_1024_train'],
        'train_target_root': dataset_paths['dem_5m_1024_train'],
        'test_source_root': dataset_paths['dem_5m_1024_test'],
        'test_target_root': dataset_paths['dem_5m_1024_test'],
    },
    'sketch_to_dem_1024': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['dem_5m_1024_train_sketch'],
        'train_target_root': dataset_paths['dem_5m_1024_train'],
        'test_source_root': dataset_paths['dem_5m_1024_test_sketch'],
        'test_target_root': dataset_paths['dem_5m_1024_test'],
    },
    'sketch_to_dem_256': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['dem_30m_256_train_sketch'],
        'train_target_root': dataset_paths['dem_30m_256_train'],
        'test_source_root': dataset_paths['dem_30m_256_test_sketch'],
        'test_target_root': dataset_paths['dem_30m_256_test'],
    },

    'dem_to_dem_30m_256': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['dem_30m_256_train'],
        'train_target_root': dataset_paths['dem_30m_256_train'],
        'test_source_root': dataset_paths['dem_30m_256_test'],
        'test_target_root': dataset_paths['dem_30m_256_test'],
    },

    'test_overfit': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['test_overfit_train'],
        'train_target_root': dataset_paths['test_overfit_train'],
        'test_source_root': dataset_paths['test_overfit_test'],
        'test_target_root': dataset_paths['test_overfit_test'],
    },

    'test_w': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['test_w_train'],
        'train_target_root': dataset_paths['test_w_train'],
        'test_source_root': dataset_paths['test_w_test'],
        'test_target_root': dataset_paths['test_w_test'],
        'train_w_root': dataset_paths['test_w_train_w'],
        'test_w_root': dataset_paths['test_w_test_w']
    },

    'test': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['test_train'],
        'train_target_root': dataset_paths['test_train'],
        'test_source_root': dataset_paths['test_test'],
        'test_target_root': dataset_paths['test_test'],
    },

    'test_adaptative': {
        'transforms': transforms_config.DEMTransformsAdaptative,
        'train_source_root': dataset_paths['test_train'],
        'train_target_root': dataset_paths['test_train'],
        'test_source_root': dataset_paths['test_test'],
        'test_target_root': dataset_paths['test_test'],
    },

    'sg2_srtm_30m_1024_4000kimg_adaptative': {
        'transforms': transforms_config.DEMTransformsAdaptative,
        'train_source_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_train'],
        'train_target_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_train'],
        'test_source_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_test'],
        'test_target_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_test'],
    },

    'sg2_srtm_30m_1024_4000kimg': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_train'],
        'train_target_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_train'],
        'test_source_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_test'],
        'test_target_root': dataset_paths['sg2_srtm_30m_1024_4000kimg_test'],
    },

    'srtm_alps_ethio': {
        'transforms': transforms_config.DEMTransforms,
        'train_source_root': dataset_paths['srtm_alps_ethio_train'],
        'train_target_root': dataset_paths['srtm_alps_ethio_train'],
        'test_source_root': dataset_paths['srtm_alps_ethio_test'],
        'test_target_root': dataset_paths['srtm_alps_ethio_test'],
    },
}
