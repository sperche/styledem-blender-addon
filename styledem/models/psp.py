import random
import math
import time

import torch
from torch import nn
import numpy as np

from .encoders import psp_encoders
from .stylegan2.model import Generator
from .stylegan2 import legacy
from ..configs.paths_config import model_paths
from ..torch_utils import misc
from .. import dnnlib


def get_keys(d, name):
    if 'state_dict' in d:
        d = d['state_dict']
    d_filt = {k[len(name) + 1:]: v for k, v in d.items() if k[:len(name)] == name}
    return d_filt


class pSp(nn.Module):

    def __init__(self, opts):
        super(pSp, self).__init__()
        self.set_opts(opts)
        # compute number of style inputs based on the output resolution
        self.opts.n_styles = int(math.log(self.opts.output_size, 2)) * 2 - 2
        # Define architecture

        self.f_res = None

        # TODO: make a parameter for img_channels
        self.decoder = Generator(z_dim=512, c_dim=0, w_dim=512, img_channels=1, img_resolution=self.opts.output_size)

        self.encoder = self.set_encoder(self.decoder.synthesis.channels_dict)

        self.face_pool = torch.nn.AdaptiveAvgPool2d((256, 256))
        # Load weights if needed
        self.load_weights()
        for param in self.encoder.gradual_style_encoder.parameters():
            param.requires_grad = False

    def set_encoder(self, channels_dict=None):
        if self.opts.encoder_type == 'GradualStyleEncoder':
            encoder = psp_encoders.GradualStyleEncoder(50, 'ir_se', self.opts)
        elif self.opts.encoder_type == 'BackboneEncoderUsingLastLayerIntoW':
            encoder = psp_encoders.BackboneEncoderUsingLastLayerIntoW(50, 'ir_se', self.opts)
        elif self.opts.encoder_type == 'BackboneEncoderUsingLastLayerIntoWPlus':
            encoder = psp_encoders.BackboneEncoderUsingLastLayerIntoWPlus(50, 'ir_se', self.opts)
        elif self.opts.encoder_type == 'CNNSkipInF':
            encoder = psp_encoders.CNNSkipInF(256, self.f_res, channels_dict, self.opts)
        elif self.opts.encoder_type == 'CNNSkipInAllF':
            encoder = psp_encoders.CNNSkipInAllF(256, channels_dict, self.opts, f_res_max=256)
        else:
            raise Exception('{} is not a valid encoders'.format(self.opts.encoder_type))
        return encoder

    def load_weights(self):
        if self.opts.checkpoint_path is not None:
            print('Loading pSp from checkpoint: {}'.format(self.opts.checkpoint_path))
            ckpt = torch.load(self.opts.checkpoint_path, map_location='cpu')
            self.encoder.load_state_dict(get_keys(ckpt, 'encoder'), strict=True)
            # self.encoder.gradual_style_encoder.load_state_dict(get_keys(ckpt, 'encoder'), strict=True)
            self.decoder.load_state_dict(get_keys(ckpt, 'decoder'), strict=True)
            self.__load_latent_avg(ckpt, from_psp=True)
        else:
            # print('Loading encoders weights from irse50!')
            # encoder_ckpt = torch.load(model_paths['ir_se50'])
            # # if input to encoder is not an RGB image, do not load the input layer weights
            # if self.opts.label_nc != 0:
            #     encoder_ckpt = {k: v for k, v in encoder_ckpt.items() if "input_layer" not in k}
            # self.encoder.load_state_dict(encoder_ckpt, strict=False)
            # print('Loading decoder weights from pretrained!')
            # with dnnlib.util.open_url(self.opts.stylegan_weights) as f:
            #     G = legacy.load_network_pkl(f)['G_ema']
            #
            # self.decoder = G
            # for param in self.decoder.parameters():
            #     param.requires_grad = False
            #
            # self.__load_latent_avg(G.state_dict(), from_psp=False, repeat=self.opts.n_styles)
            print('Loading decoder weights from pretrained!')
            with dnnlib.util.open_url(self.opts.stylegan_weights) as f:
                G = legacy.load_network_pkl(f)['G_ema']
            misc.copy_params_and_buffers(G, self.decoder, require_all=False, require_grad=False)

            # self.decoder = G
            for param in self.decoder.parameters():
                param.requires_grad = False

            self.__load_latent_avg(G.state_dict(), from_psp=False, repeat=self.opts.n_styles)

    def forward(self, x, resize=True, latent_mask=None, input_code=False, randomize_noise=True,
                inject_latent=None, return_latents=False, alpha=None, mask=None, only_get_f_img_and_w=False):
        f_maps = None
        # self.f_res = random.choice([8, 16, 32, 64, 128])
        if input_code:
            codes = x
        else:
            #start = time.time()
            if self.opts.encoder_type == 'CNNSkipInAllF':
                codes = self.encoder(x, f_res=self.f_res)
            else:
                codes = self.encoder(x)
            #print(f'Encoder : {time.time() - start}')

            if self.opts.encoder_type in ['CNNSkipInF', 'CNNSkipInAllF']:
                f_maps, codes = codes

            if self.opts.start_from_latent_avg:
                if self.opts.learn_in_w:
                    codes = codes + self.latent_avg.repeat(codes.shape[0], 1)
                else:
                    codes = codes + self.latent_avg.repeat(codes.shape[0], 1, 1)

        if latent_mask is not None:
            for i in latent_mask:
                if inject_latent is not None:
                    if alpha is not None:
                        codes[:, i] = alpha * inject_latent[:, i] + (1 - alpha) * codes[:, i]
                    else:
                        codes[:, i] = inject_latent[:, i]
                else:
                    codes[:, i] = 0

        result_latent = codes
        #start = time.time()

        f_maps_and_img = None
        if f_maps is not None:
            f_maps_and_img = [f_maps, nn.functional.interpolate(x, scale_factor=1 / (
                        2 ** (np.log2(x.shape[-1]) - np.log2(self.f_res) + 1)), mode='area')]
            
        if only_get_f_img_and_w:
            return result_latent, f_maps_and_img

        images = self.decoder.synthesis(codes, noise_mode='random' if randomize_noise else 'const',
                                        f_maps_and_img=f_maps_and_img, start_at=self.f_res, mask=mask)
        #print(f'Generator : {time.time() - start}')

        if resize:
            images = self.face_pool(images)

        if return_latents:
            return images, result_latent
        else:
            return images

    def set_opts(self, opts):
        self.opts = opts

    def __load_latent_avg(self, ckpt, from_psp=False, repeat=None):
        if from_psp:
            weight_name = 'latent_avg'
        else:
            weight_name = 'mapping.w_avg'
        if weight_name in ckpt:
            self.latent_avg = ckpt[weight_name].to(self.opts.device)
            if repeat is not None:
                self.latent_avg = self.latent_avg.repeat(repeat, 1)
        else:
            self.latent_avg = None
