import math

import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
from torch.nn import Linear, Conv2d, BatchNorm2d, PReLU, Sequential, Module

from .helpers import get_blocks, Flatten, bottleneck_IR, bottleneck_IR_SE
# from models.encoders.op.fused_act import fused_leaky_relu


class EqualLinear(nn.Module):
    def __init__(
            self, in_dim, out_dim, bias=True, bias_init=0, lr_mul=1, activation=None
    ):
        super().__init__()

        self.weight = nn.Parameter(torch.randn(out_dim, in_dim).div_(lr_mul))

        if bias:
            self.bias = nn.Parameter(torch.zeros(out_dim).fill_(bias_init))

        else:
            self.bias = None

        self.activation = activation

        self.scale = (1 / math.sqrt(in_dim)) * lr_mul
        self.lr_mul = lr_mul

    def forward(self, input):
        if self.activation:
            raise NotImplementedError('Should not happen')
            # out = F.linear(input, self.weight * self.scale)
            # out = fused_leaky_relu(out, self.bias * self.lr_mul)

        else:
            out = F.linear(
                input, self.weight * self.scale, bias=self.bias * self.lr_mul
            )

        return out

    def __repr__(self):
        return (
            f'{self.__class__.__name__}({self.weight.shape[1]}, {self.weight.shape[0]})'
        )


class GradualStyleBlock(Module):
    def __init__(self, in_c, out_c, spatial):
        super(GradualStyleBlock, self).__init__()
        self.out_c = out_c
        self.spatial = spatial
        num_pools = int(np.log2(spatial))
        modules = []
        modules += [Conv2d(in_c, out_c, kernel_size=3, stride=2, padding=1),
                    nn.LeakyReLU()]
        for i in range(num_pools - 1):
            modules += [
                Conv2d(out_c, out_c, kernel_size=3, stride=2, padding=1),
                nn.LeakyReLU()
            ]
        self.convs = nn.Sequential(*modules)
        self.linear = EqualLinear(out_c, out_c, lr_mul=1)

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self.out_c)
        x = self.linear(x)
        return x


class GradualStyleEncoder(Module):
    def __init__(self, num_layers, mode='ir', opts=None):
        super(GradualStyleEncoder, self).__init__()
        assert num_layers in [50, 100, 152], 'num_layers should be 50,100, or 152'
        assert mode in ['ir', 'ir_se'], 'mode should be ir or ir_se'
        blocks = get_blocks(num_layers)
        if mode == 'ir':
            unit_module = bottleneck_IR
        elif mode == 'ir_se':
            unit_module = bottleneck_IR_SE
        self.input_layer = Sequential(Conv2d(opts.input_nc, 64, (3, 3), 1, 1, bias=False),
                                      BatchNorm2d(64),
                                      PReLU(64))
        modules = []
        for block in blocks:
            for bottleneck in block:
                modules.append(unit_module(bottleneck.in_channel,
                                           bottleneck.depth,
                                           bottleneck.stride))
        self.body = Sequential(*modules)

        self.styles = nn.ModuleList()
        self.style_count = opts.n_styles
        self.coarse_ind = 3
        self.middle_ind = 7
        for i in range(self.style_count):
            if i < self.coarse_ind:
                style = GradualStyleBlock(512, 512, 16)
            elif i < self.middle_ind:
                style = GradualStyleBlock(512, 512, 32)
            else:
                style = GradualStyleBlock(512, 512, 64)
            self.styles.append(style)
        self.latlayer1 = nn.Conv2d(256, 512, kernel_size=1, stride=1, padding=0)
        self.latlayer2 = nn.Conv2d(128, 512, kernel_size=1, stride=1, padding=0)

    def _upsample_add(self, x, y):
        '''Upsample and add two feature maps.
        Args:
          x: (Variable) top feature map to be upsampled.
          y: (Variable) lateral feature map.
        Returns:
          (Variable) added feature map.
        Note in PyTorch, when input size is odd, the upsampled feature map
        with `F.upsample(..., scale_factor=2, mode='nearest')`
        maybe not equal to the lateral feature map size.
        e.g.
        original input size: [N,_,15,15] ->
        conv2d feature map size: [N,_,8,8] ->
        upsampled feature map size: [N,_,16,16]
        So we choose bilinear upsample which supports arbitrary output sizes.
        '''
        _, _, H, W = y.size()
        return F.interpolate(x, size=(H, W), mode='bilinear', align_corners=True) + y

    def forward(self, x):
        x = self.input_layer(x)

        latents = []
        modulelist = list(self.body._modules.values())
        for i, l in enumerate(modulelist):
            x = l(x)
            if i == 6:
                c1 = x
            elif i == 20:
                c2 = x
            elif i == 23:
                c3 = x

        for j in range(self.coarse_ind):
            latents.append(self.styles[j](c3))

        p2 = self._upsample_add(c3, self.latlayer1(c2))
        for j in range(self.coarse_ind, self.middle_ind):
            latents.append(self.styles[j](p2))

        p1 = self._upsample_add(p2, self.latlayer2(c1))
        for j in range(self.middle_ind, self.style_count):
            latents.append(self.styles[j](p1))

        out = torch.stack(latents, dim=1)
        return out


class BackboneEncoderUsingLastLayerIntoW(Module):
    def __init__(self, num_layers, mode='ir', opts=None):
        super(BackboneEncoderUsingLastLayerIntoW, self).__init__()
        print('Using BackboneEncoderUsingLastLayerIntoW')
        assert num_layers in [50, 100, 152], 'num_layers should be 50,100, or 152'
        assert mode in ['ir', 'ir_se'], 'mode should be ir or ir_se'
        blocks = get_blocks(num_layers)
        if mode == 'ir':
            unit_module = bottleneck_IR
        elif mode == 'ir_se':
            unit_module = bottleneck_IR_SE
        self.input_layer = Sequential(Conv2d(opts.input_nc, 64, (3, 3), 1, 1, bias=False),
                                      BatchNorm2d(64),
                                      PReLU(64))
        self.output_pool = torch.nn.AdaptiveAvgPool2d((1, 1))
        self.linear = EqualLinear(512, 512, lr_mul=1)
        modules = []
        for block in blocks:
            for bottleneck in block:
                modules.append(unit_module(bottleneck.in_channel,
                                           bottleneck.depth,
                                           bottleneck.stride))
        self.body = Sequential(*modules)

    def forward(self, x):
        x = self.input_layer(x)
        x = self.body(x)
        x = self.output_pool(x)
        x = x.view(-1, 512)
        x = self.linear(x)
        return x


class BackboneEncoderUsingLastLayerIntoWPlus(Module):
    def __init__(self, num_layers, mode='ir', opts=None):
        super(BackboneEncoderUsingLastLayerIntoWPlus, self).__init__()
        print('Using BackboneEncoderUsingLastLayerIntoWPlus')
        assert num_layers in [50, 100, 152], 'num_layers should be 50,100, or 152'
        assert mode in ['ir', 'ir_se'], 'mode should be ir or ir_se'
        blocks = get_blocks(num_layers)
        if mode == 'ir':
            unit_module = bottleneck_IR
        elif mode == 'ir_se':
            unit_module = bottleneck_IR_SE
        self.n_styles = opts.n_styles
        self.input_layer = Sequential(Conv2d(opts.input_nc, 64, (3, 3), 1, 1, bias=False),
                                      BatchNorm2d(64),
                                      PReLU(64))
        self.output_layer_2 = Sequential(BatchNorm2d(512),
                                         torch.nn.AdaptiveAvgPool2d((7, 7)),
                                         Flatten(),
                                         Linear(512 * 7 * 7, 512))
        self.linear = EqualLinear(512, 512 * self.n_styles, lr_mul=1)
        modules = []
        for block in blocks:
            for bottleneck in block:
                modules.append(unit_module(bottleneck.in_channel,
                                           bottleneck.depth,
                                           bottleneck.stride))
        self.body = Sequential(*modules)

    def forward(self, x):
        x = self.input_layer(x)
        x = self.body(x)
        x = self.output_layer_2(x)
        x = self.linear(x)
        x = x.view(-1, self.n_styles, 512)
        return x


class BlockSpatialToCode(Module):
    def __init__(self, in_size, out_size, input_channels, output_channel):
        super().__init__()
        nb_reduc = int(np.log2(in_size / out_size))
        first_channels = max(1, output_channel // (2 ** nb_reduc))
        self.input_layer = Sequential(Conv2d(input_channels, first_channels, kernel_size=3, padding=1),
                                      BatchNorm2d(first_channels),
                                      PReLU(first_channels))

        self.block_channels = []

        for i in range(nb_reduc):
            in_channels = min(first_channels * 2 ** i, output_channel)
            out_channels = min(in_channels * 2, output_channel)

            # https://arxiv.org/abs/1603.05027
            block = [BatchNorm2d(in_channels),
                     PReLU(in_channels),
                     Conv2d(in_channels, in_channels, kernel_size=3, padding=1),
                     BatchNorm2d(in_channels),
                     PReLU(in_channels),
                     Conv2d(in_channels, out_channels, kernel_size=3, padding=1),
                     ]

            block = Sequential(*block)

            # Perform a projection shorcut as defined in https://arxiv.org/pdf/1512.03385.pdf
            proj_shortcut = Conv2d(in_channels, out_channels, kernel_size=1, padding=0)
            adapt_avg_pool_2d = nn.AvgPool2d(2, stride=2)

            setattr(self, f'b_{in_channels}_to_{out_channels}', block)
            setattr(self, f'proj_shortcut_{in_channels}_to_{out_channels}', proj_shortcut)
            setattr(self, f'adapt_avg_pool_2d_{in_channels}_to_{out_channels}', adapt_avg_pool_2d)

            self.block_channels.append((in_channels, out_channels))

    def forward(self, x):
        x = self.input_layer(x)
        for in_channels, out_channels in self.block_channels:
            block = getattr(self, f'b_{in_channels}_to_{out_channels}')
            skip = block(x)

            proj_shortcut = getattr(self, f'proj_shortcut_{in_channels}_to_{out_channels}')
            x = proj_shortcut(x)

            x = x + skip

            adapt_avg_pool_2d = getattr(self, f'adapt_avg_pool_2d_{in_channels}_to_{out_channels}')
            x = adapt_avg_pool_2d(x)
        return x


class CNNSkipInF(nn.Module):
    def __init__(self, input_size, f_res, channels_dict, opts):
        super().__init__()

        self.to_f_maps = BlockSpatialToCode(input_size, f_res, 1, channels_dict[f_res])
        self.gradual_style_encoder = GradualStyleEncoder(50, 'ir_se', opts)
        # self.to_w = nn.ModuleList([BlockSpatialToCode(input_size, 1, 1, w_dim)])

        # for res, _ in channel_dict.items():
        #     if f_res < res <= stop_at:
        #         self.to_w += [BlockSpatialToCode(input_size, 1, 1, w_dim),
        #                       BlockSpatialToCode(input_size, 1, 1, w_dim)]

    def forward(self, x):
        f_maps = self.to_f_maps(x)

        codes = self.gradual_style_encoder(x)

        # codes = []
        # for w in self.to_w:
        #     codes += [w(x.clone())]
        #
        # while len(codes) < 18:
        #     codes = [torch.empty(0).to(x.device)] + codes

        return [f_maps, codes]


class CNNSkipInAllF(nn.Module):
    def __init__(self, input_size, channels_dict, opts, f_res_max=None):
        super().__init__()

        for f_res in channels_dict.keys():
            if f_res_max is not None and f_res > f_res_max:
                continue
            setattr(self, f'to_f_maps_{f_res}', BlockSpatialToCode(input_size, f_res, 1, channels_dict[f_res]))

        # self.to_f_maps = BlockSpatialToCode(input_size, f_res, 1, channels_dict[f_res])
        self.gradual_style_encoder = GradualStyleEncoder(50, 'ir_se', opts)
        # self.to_w = nn.ModuleList([BlockSpatialToCode(input_size, 1, 1, w_dim)])

        # for res, _ in channel_dict.items():
        #     if f_res < res <= stop_at:
        #         self.to_w += [BlockSpatialToCode(input_size, 1, 1, w_dim),
        #                       BlockSpatialToCode(input_size, 1, 1, w_dim)]

    def forward(self, x, f_res=None):
        f_maps = None
        if f_res is not None:
            f_maps = getattr(self, f'to_f_maps_{f_res}')(x)
        # f_maps = self.to_f_maps(x)

        codes = self.gradual_style_encoder(x)

        # codes = []
        # for w in self.to_w:
        #     codes += [w(x.clone())]
        #
        # while len(codes) < 18:
        #     codes = [torch.empty(0).to(x.device)] + codes

        return [f_maps, codes]
