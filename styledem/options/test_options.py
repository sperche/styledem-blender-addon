from argparse import ArgumentParser

from ...utils import dotdict

class TestOptions:

	def __init__(self):
		# self.parser = ArgumentParser()
		self.opts = dotdict()
		self.initialize()
		

	def initialize(self):
		# Default argument without ArgumentParser object
		self.opts.checkpoint_path = None
		self.opts.data_path = 'gt_images'
		self.opts.couple_outputs = False
		self.opts.resize_outputs = False
		self.opts.dataset_type = 'ffhq_encode'
		self.opts.randomize_noise = False
		self.opts.test_batch_size = 2
		self.opts.test_workers = 2
		self.opts.n_images = None
		self.opts.n_outputs_to_generate = 5
		self.opts.mix_alpha = None
		self.opts.latent_mask = None
		self.opts.resize_factors = None
		self.opts.sr_grid = 6

	def get_opts(self):
		return self.opts
