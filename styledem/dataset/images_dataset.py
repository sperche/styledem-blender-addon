from copy import deepcopy

from torch.utils.data import Dataset
from PIL import Image
from utils import data_utils
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms


class ImagesDataset(Dataset):

    def __init__(self, source_root, target_root, opts, target_transform=None, source_transform=None, w_root=None):
        self.source_paths = sorted(data_utils.make_dataset(source_root))
        self.target_paths = sorted(data_utils.make_dataset(target_root))

        self.w_paths = None
        if w_root is not None:
            self.w_paths = sorted(data_utils.make_dataset(w_root))

        self.source_transform = source_transform
        self.target_transform = target_transform
        self.opts = opts

    def __len__(self):
        return len(self.source_paths)

    def __getitem__(self, index):
        from_path = self.source_paths[index]
        from_im = Image.open(from_path)
        # from_im = from_im.convert('RGB') if self.opts.label_nc == 0 else from_im.convert('L')

        to_path = self.target_paths[index]
        to_im = Image.open(to_path)  #.convert('RGB')

        to_im_orig = deepcopy(to_im)
        to_im_orig = transforms.ToTensor()(to_im_orig).float()

        if self.target_transform:
            to_im = self.target_transform(to_im)

        if to_im.dtype != torch.float64:
            to_im = to_im.float()

        to_im = (to_im-torch.min(to_im)) / (torch.max(to_im) - torch.min(to_im))
        # to_im = torchvision.transforms.Normalize([0.5], [0.5])(to_im)

        to_im_orig = (to_im_orig - torch.min(to_im_orig)) / (torch.max(to_im_orig) - torch.min(to_im_orig))

        if self.source_transform:
            from_im = self.source_transform(from_im)
            if from_im.dtype != torch.float64:
                from_im = from_im.float()
            from_im = (from_im - torch.min(from_im)) / (torch.max(from_im) - torch.min(from_im))
            # from_im = torchvision.transforms.Normalize([0.5], [0.5])(from_im)
        else:
            from_im = to_im

        # if to_im.shape[0] != 3 and len(to_im.shape) == 3:
        # 	to_im = to_im.repeat(3, 1, 1)

        if self.opts.label_nc == 0:
            from_im = (from_im - torch.min(from_im)) / (torch.max(from_im) - torch.min(from_im))
            if from_im.shape[0] != 3 and len(from_im.shape) == 3:
                from_im = from_im.repeat(3, 1, 1)

        to_latent = torch.empty(0)
        if self.w_paths is not None:
            latent_path = self.w_paths[index]
            to_latent = torch.tensor(np.load(latent_path)['w'])

        return from_im, to_im, to_latent, to_im_orig
