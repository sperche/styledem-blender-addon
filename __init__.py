import os
import sys

# Allow direct execution
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))  # noqa
sys.path.insert(0, os.getcwd())  # noqa
from . import panels
from . import operators
from .utils import Logger
try:
    import bpy
    

    classes = (
        # Order define the panels' order in the UI
        panels.MainPanel,
        panels.InversionPanel,
        panels.StyleMixingPanel,

        operators.inversion.InvertImage,
        operators.inversion.InversionProperty,

        operators.style.StyleMixing,
        operators.style.StyleMixingProperty,

        operators.miscellaneous.OpenFileBrowser,
        operators.miscellaneous.LogFolder,
    )
except ImportError as e:
    print(str(e))
    print('"bpy" module not found. This may not be an issue if you are running the server.')


bl_info = {
    "name": "StyleDEM",
    "author": "Simon Perche",
    "description": "",
    "blender": (3, 0, 0),
    "version": (0, 1, 0),
    "location": "View3D",
    "warning": "",
    "category": "Terrain authoring"
}


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.inversion_props = bpy.props.PointerProperty(type=operators.inversion.InversionProperty)
    bpy.types.Scene.stylemixing_props = bpy.props.PointerProperty(type=operators.style.StyleMixingProperty)


def unregister():
    Logger().log('Log session ended.')

    del bpy.types.Scene.inversion_props
    del bpy.types.Scene.stylemixing_props

    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()
